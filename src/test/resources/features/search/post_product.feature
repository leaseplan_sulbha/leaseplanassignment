Feature: Search for the product

  ### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
  ### Available products: "orange", "apple", "pasta", "cola"
  ### Prepare Positive and negative scenarios
  @positive
  Scenario Outline: Verifying available products with related result
    When he calls endpoint <product>
    Then he gets correct response code
    Then he sees the results displayed for <product>

    Examples: 
      | product |
      | apple   |
      | pasta   |
      | cola    |

  @positive
  Scenario Outline: Verifying products with only empty response and correct status
    When he calls endpoint <product>
    Then he gets correct response code

    Examples: 
      | product |
      | orange  |

  @negative
  Scenario Outline: Verifying invalid products
    When he calls endpoint <product>
    Then he doesn not see the results

    Examples: 
      | product |
      | car     |
