package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.*;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;

public class SearchStepDefinitions {

	String endPoint = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";

	@When("^he calls endpoint (.*)$")
	public void heCallsEndpoint(String product) {
		SerenityRest.given().get(endPoint + product);
	}

	@Then("he gets correct response code")
	public void heGetsCorrectResponseCode() {
		restAssuredThat(response -> response.statusCode(200));
	}

	@Then("^he sees the results displayed for (.*)$")
	public void heSeesTheResultsDisplayedForProduct(String product) {
		restAssuredThat(response -> response.body("title", hasItem(containsStringIgnoringCase(product))));
	}

	@Then("he doesn not see the results")
	public void he_Doesn_Not_See_The_Results() {
		restAssuredThat(response -> response.body("detail.error", equalTo(true)));
	}
}
