## Instructions to Install, Run and Write new test cases
- A google drive link was given to me, and I used it to download the zip file and unzip the project to my local drive. 
- Next, I imported the maven project into the Eclipse IDE and attempted to execute the pre-existing framework using the "maven clean verify" option in the Eclipse project run as window.
- After that, I began to update the feature file to keep up with the need, refactoring Step Defination in order to make it more dynamic rather than using hard-coded values.
- I created two positive and one negative scenario.
- Next, I added the .gitlab-ci.yml file to start the CI/CD workflow after pushing my functional code into Gitlab. In order to start the pipeline, I used Gitlab shared runners.
- Once the build was succesfull, I updated my readMe file and pushed the complete code to Gitlab repository.


## Gitlab report
- Go to CI/CD --> Pipelines, select the pipeline --> test tab, the online junit report will be there.
- Go to download artifact, open serenity report.

## My Update of this assignment:
- Remove gradle setting: Since we will use Maven
- Restructure code: feature file and StepDefination file: to make the code more dynamic w.r.t. test cases
- Removed unused code like CarsApi and gradle configuration
- Test suite and test scenarios: for better coverage
- Added .gitlab-ci.yml file: for CI/CD


## Requirement in feature file 
- Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
- Available products: "orange", "apple", "pasta", "cola"
- Prepare Positive and negative scenarios
  
